# How to install zsh ?

1) Install zsh
```
sudo apt install -y zsh
chsh -s $(which zsh)
sudo reboot
```
Log out and log back in again to use your new default shell.
If doesn't work -> reboot

2) Install ho my zsh
```
sh -c "$(wget https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh -O -)"
```

3) Install powerlevel10k
```
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ~/powerlevel10k
echo 'source ~/powerlevel10k/powerlevel10k.zsh-theme' >>~/.zshrc
```
Close your terminal and open a new one. Customise your shel according to instructions.

4) Install my favorites plugins:
```
pip install thefuck
echo "export PATH=$PATH:~/.local/bin" >> ~/.zshrc

sudo apt install -y fzf zsh-syntax-highlighting zsh-autosuggestions

cp -r ./plugins/* $ZSH_CUSTOM/plugins/
```
And add this to your ~/.zshrc plugins:
```
plugins=(git zsh-autosuggestions ag thefuck fzf zsh-syntax-highlighting zsh-you-should-use docker docker-compose z sudo themes command-not-found web-search)
```